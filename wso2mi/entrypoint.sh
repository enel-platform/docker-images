#!/bin/bash

ARTIFACTORY_USERNAME=$1
ARTIFACTORY_PASSWORD=$2
COMMON_MEDIATION_COMPOSITE_APPLICATION_VERSION=$3
SYNAPSE_PLATFORM_SDK_VERSION=$4

curl -s -u "${ARTIFACTORY_USERNAME}:${ARTIFACTORY_PASSWORD}" -X GET "https://artifactory.springlab.enel.com/artifactory/local-enel-releases/com/enel/platform/wso2/CommonMediationCompositeApplication/${COMMON_MEDIATION_COMPOSITE_APPLICATION_VERSION}/CommonMediationCompositeApplication-${COMMON_MEDIATION_COMPOSITE_APPLICATION_VERSION}.car" -o "${WSO2_SERVER_HOME}/repository/deployment/server/carbonapps/CommonMediationCompositeApplication-${COMMON_MEDIATION_COMPOSITE_APPLICATION_VERSION}.car"
curl -s -u "${ARTIFACTORY_USERNAME}:${ARTIFACTORY_PASSWORD}" -X GET "https://artifactory.springlab.enel.com/artifactory/local-enel-releases/com/enel/platform/synapse-platform-sdk/${SYNAPSE_PLATFORM_SDK_VERSION}/synapse-platform-sdk-${SYNAPSE_PLATFORM_SDK_VERSION}.jar" -o "${WSO2_SERVER_HOME}/dropins/synapse-platform-sdk-${SYNAPSE_PLATFORM_SDK_VERSION}.jar"

cp /tmp/*.car "${WSO2_SERVER_HOME}/repository/deployment/server/carbonapps"

/home/wso2carbon/docker-entrypoint.sh